### 安装步骤
* git clone https://gitee.com/1967988842/Spider2.0.git
* 使用eclipse导入项目
* 直接运行SpiderApplication里面的main方法就是提供的抓取携程网站北京地区的所有酒店信息

### 依赖库
* JCEF
* Jsoup
* apache-commons-*类库

### 编译JCEF（想要编译完整文件可以直接私信我）
* 准备环境

> Windows10 64bit操作系统，Visio Studio 2017（安装c++组件），Java8 64bit，Python2.7，cmake3.11.3-win

* 下载代码到指定目录

> git clone https://bitbucket.org/chromiumembedded/java-cef.git src     

* 创建并进入指定文件夹 

> mkdir jcef_build && cd jcef_bui

* 使用cmake创建VS工程  

> cmake -G "Visual Studio 15 Win64"

* 网络错误

> 有可能会因为网络原因出现上边的下载错误
> 把gs://chromium-clang-format/6ddedd571c56b8c184f30a3c1fc36984e8c10ccd  
> 改为https://storage.googleapis.com/chromium-clang-format/6ddedd571c56b8c184f30a3c1fc36984e8c10ccd  
> 然后使用浏览器下载，下载完成后再把文件名改为clang-format.exe放到tools/buildtools/win目录  

* 使用VS2017打开jcef_build文件夹中的jcef.sln  

* 修改解决方案类型 

> 在VS2017中选择生成->配置管理器 ->活动解决方案配置,修改为"Releas

* 生成解决方案   
 
> 在VS2017中选择生成->创建解决

* 进入到tools目录下执行命令编译Java文件   

> compile.bat win

* 进入到tools目录下执行命令测试是否编译成功   

> run.bat win64 Release detaile

* 进入到tools目录下执行命令创建Jar包和打包DLL文件   

> make_distrib.bat win64 

### 在eclipse中使用使用
* 参考项目中lib包下的文件，可以将新打包的文件拷贝到lib目录下。并将Jar包添加到项目的Build Pathh中
* 修改common包下Constant类中的DLLPATH值

> 修改为当前项目的或者是打包完成的目录地址：D:/Products/smallSpider/new/src/binary_distrib/win64/bin/lib/win

* 在JCEF中提供了两个例子simple(java/simple/MainFrame.java)或detailed(java/detailed/MainFrame.java)。可以查看项目的test包下面查看并且运行

### 关于爬虫

> 此类爬虫局限性特别大。对于实在无法抓取的网页数据。例如Ajax请求参数复杂，无法通过httpclient获取json数据；或者需要加载复杂js，无法通过使用phantomjs等无头浏览器加载页面；又或者使用selenium嫌弃太慢。 那么此项目可能会为您提供帮助

### 警告

> <b>此项目中抓取携程酒店数据的例子请谨慎使用</b>
   

### 代码解释
* SpiderApplication.java是程序的入口。创建浏览器实例并且设置起始页和结束页进行抓取。    

```java
//新建一个浏览器
UrlBrowserInstance browserInstanceFirst = new UrlBrowserInstance(client, osrEnabledArg, transparentPaintingEnabledArg);
//设置起始页和结束页开始抓取
browserInstanceFirst.createBrowserAndCrawler(1L, 20L);
```
* common包

> 队列和一些常量以及共用方法，通常情况下可以多关注一下队列类就足够了

* controller包

> 实现了浏览器接口。一般来说列表页跳转下一页有两种方式，一种是URL改变。一种是页面不重新刷新，使用Ajax加载数据。此部分代码在impl包下面，需要根据网站进行修改。parase包下是两种处理页面方式，下载页面之后处理或者直接进行处理生成csv文件或者保存数据库。使用Jsoup解析Html文档即可。非常简单

```java
//浏览器实例需要实现接口方法，可以创建多个浏览器实例
public interface BrowserInstance {
	void createBrowserAndCrawler(Long startIndex, Long stopIndex);
}
```
```java
//下载Html页面示例代码
public class DownloadHtmlDocument implements CefStringVisitor {
	private String url;
	public DownloadHtmlDocument(String url) {
		this.url = url;
	}
	@Override
	public void visit(String string) {
		StringBuilder savePath = new StringBuilder("/home/ctrip/");
		String index = StringUtils.remove(url, Constant.CTRIPHOTELLISTUTLTEMP);
		savePath.append(index).append(".txt");
		File file = new File(savePath.toString());
		try {
			if (!file.exists()) {
				FileUtils.writeStringToFile(file, string, "UTF-8", false);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
```
```java
//解析Html页面示例代码（未完成）
public class ParsingHtmlDocument implements CefStringVisitor {
	private Map<String, String> xpathList;
	public ParsingHtmlDocument(Map<String, String> xpathList) {
		this.xpathList = xpathList;
	}
	@Override
	public void visit(String string) {
		Document htmlDocument = Jsoup.parse(string);
		System.out.println(htmlDocument);
		System.out.println(xpathList);
	}
}
```
* core包

> 核心包，其中的类以后会打入到Jar包中。与业务无关

* ui包

> 使用swing制作的浏览器Frame。几乎照搬JCEF提供的detail例子


