package com.leadingsoft;

import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.CefSettings;
import org.cef.OS;

import com.leadingsoft.controller.impl.UrlBrowserInstance;
import com.leadingsoft.core.AddLibraryDir;

/**
 * @ClassName Application
 * @Description 程序的起点
 * @author gongym
 * @date 2018年6月5日 下午6:27:26
 */
public class SpiderApplication {
	public static void main(String[] args) {
		AddLibraryDir.addDLL();
		boolean osrEnabledArg = OS.isLinux();
		boolean transparentPaintingEnabledArg = false;
		CefSettings settings = new CefSettings();
		settings.windowless_rendering_enabled = osrEnabledArg;
		settings.background_color = settings.new ColorType(100, 255, 242, 211);
		CefApp myApp = CefApp.getInstance(settings);
		final CefClient client = myApp.createClient();
		// 新建一个浏览器实例进行抓取
		UrlBrowserInstance browserInstanceFirst = new UrlBrowserInstance(client, osrEnabledArg,
				transparentPaintingEnabledArg);
		browserInstanceFirst.createBrowserAndCrawler(1L, 20L);
	}
}
