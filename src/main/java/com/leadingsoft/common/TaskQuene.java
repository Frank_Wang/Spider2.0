package com.leadingsoft.common;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @ClassName TaskQuene
 * @Description 任务队列
 * @author gongym
 * @date 2018年6月6日 下午2:54:31
 */
public class TaskQuene {
	public static LinkedBlockingQueue<String> taskUrl = new LinkedBlockingQueue<String>();
	// public static ConcurrentLinkedQueue<String> taskUrl = new ConcurrentLinkedQueue<String>();

	/**
	 * @Title getCtripHotelListUrl
	 * @Description 携程酒店北京地区的酒店列表URL
	 * @param place
	 * @param pagrIndex
	 * @return
	 * @return String
	 */
	public static String getCtripHotelListUrl(String place, Long pagrIndex) {
		StringBuffer url = new StringBuffer("http://hotels.ctrip.com/hotel/");
		url.append(place).append("/").append("p").append(pagrIndex);
		return url.toString();
	}
	/**
	 * @Title getCtripHotelCommentListUrl
	 * @Description 携程酒店北京地区的酒店评论列表URL
	 * @param hotelId
	 * @param pagrIndex
	 * @return
	 * @return String
	 */
	public static String getCtripHotelCommentListUrl(String hotelId, Long pagrIndex) {
		StringBuffer url = new StringBuffer("http://hotels.ctrip.com/hotel/dianping/");
		url.append(hotelId).append("_p").append(pagrIndex).append("t0.html");
		return url.toString();
	}
}
