package com.leadingsoft.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cef.CefClient;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.callback.CefContextMenuParams;
import org.cef.callback.CefMenuModel;
import org.cef.handler.CefContextMenuHandlerAdapter;
import org.cef.handler.CefDisplayHandlerAdapter;
import org.cef.handler.CefLoadHandlerAdapter;

/**
 * @ClassName BrowserFrame
 * @Description 单纯的浏览器窗口，只负责显示页面
 * @author gongym
 * @date 2018年6月7日 下午5:08:58
 */
public class BrowserFrame extends JFrame {
	private static final long serialVersionUID = -3449322629927619782L;
	private final CefBrowser browser;
	private String errorMsg = "";
	private ControlPanel controlPane;
	private StatusPanel statusPanel;

	public BrowserFrame(CefClient nowClient, CefBrowser nowBrowser) {
		browser = nowBrowser;
		nowClient.addContextMenuHandler(new CefContextMenuHandlerAdapter() {
			// 清理右键菜单
			@Override
			public void onBeforeContextMenu(CefBrowser browser, CefFrame frame, CefContextMenuParams params,
					CefMenuModel model) {
				model.clear();
			}
		});
		nowClient.addDisplayHandler(new CefDisplayHandlerAdapter() {
			@Override
			public void onAddressChange(CefBrowser browser, CefFrame frame, String url) {
				controlPane.setAddress(browser, url);
			}
			@Override
			public void onTitleChange(CefBrowser browser, String title) {
				setTitle(title);
			}
			@Override
			public void onStatusMessage(CefBrowser browser, String value) {
				statusPanel.setStatusText(value);
			}
		});
		nowClient.addLoadHandler(new CefLoadHandlerAdapter() {
			@Override
			public void onLoadingStateChange(CefBrowser browser, boolean isLoading, boolean canGoBack,
					boolean canGoForward) {
				controlPane.update(browser, isLoading, canGoBack, canGoForward);
				statusPanel.setIsInProgress(isLoading);
				if (!isLoading && !errorMsg.isEmpty()) {
					browser.loadString(errorMsg, controlPane.getAddress());
					errorMsg = "";
				}
			}
			@Override
			public void onLoadError(CefBrowser browser, CefFrame frame, ErrorCode errorCode, String errorText,
					String failedUrl) {
				if (errorCode != ErrorCode.ERR_NONE && errorCode != ErrorCode.ERR_ABORTED) {
					errorMsg = "<html><head>";
					errorMsg += "<title>Error while loading</title>";
					errorMsg += "</head><body>";
					errorMsg += "<h1>" + errorCode + "</h1>";
					errorMsg += "<h3>Failed to load " + failedUrl + "</h3>";
					errorMsg += "<p>" + (errorText == null ? "" : errorText) + "</p>";
					errorMsg += "</body></html>";
					browser.stopLoad();
				}
			}
		});
		getContentPane().add(createContentPanel(), BorderLayout.CENTER);
	}
	private JPanel createContentPanel() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		controlPane = new ControlPanel(browser);
		statusPanel = new StatusPanel();
		contentPanel.add(controlPane, BorderLayout.NORTH);
		contentPanel.add(browser.getUIComponent(), BorderLayout.CENTER);
		contentPanel.add(statusPanel, BorderLayout.SOUTH);
		return contentPanel;
	}
}
