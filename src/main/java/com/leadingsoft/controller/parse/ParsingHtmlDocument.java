package com.leadingsoft.controller.parse;

import java.util.Map;

import org.cef.callback.CefStringVisitor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class ParsingHtmlDocument implements CefStringVisitor {
	private Map<String, String> xpathList;

	public ParsingHtmlDocument(Map<String, String> xpathList) {
		this.xpathList = xpathList;
	}
	@Override
	public void visit(String string) {
		Document htmlDocument = Jsoup.parse(string);
		System.out.println(htmlDocument);
		System.out.println(xpathList);
	}
}
