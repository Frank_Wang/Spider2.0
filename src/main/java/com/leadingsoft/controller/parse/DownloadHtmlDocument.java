package com.leadingsoft.controller.parse;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.cef.callback.CefStringVisitor;

import com.leadingsoft.common.Constant;

public class DownloadHtmlDocument implements CefStringVisitor {
	private String url;

	public DownloadHtmlDocument(String url) {
		this.url = url;
	}
	@Override
	public void visit(String string) {
		StringBuilder savePath = new StringBuilder("/home/ctrip/");
		String index = StringUtils.remove(url, Constant.CTRIPHOTELLISTUTLTEMP);
		savePath.append(index).append(".txt");
		File file = new File(savePath.toString());
		try {
			if (!file.exists()) {
				FileUtils.writeStringToFile(file, string, "UTF-8", false);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
