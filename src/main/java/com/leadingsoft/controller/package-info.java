/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title package-info.java 
 * @Prject Spider2.0
 * @Package com.leadingsoft.controller 
 * @Description 新版爬虫，使用Java调用浏览器引擎
 * @author gongym   
 * @date 2018年6月5日 下午4:21:58 
 * @version V1.0   
 */
/**
 * @ClassName package-info
 * @Description 统一处理控制器
 * @author gongym
 * @date 2018年6月5日 下午4:21:58
 */
package com.leadingsoft.controller;
