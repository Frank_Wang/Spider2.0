package com.leadingsoft.controller.impl;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import org.apache.commons.lang3.StringUtils;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.handler.CefLoadHandlerAdapter;

import com.leadingsoft.common.CheckUrl;
import com.leadingsoft.common.Constant;
import com.leadingsoft.common.TaskQuene;
import com.leadingsoft.controller.BrowserInstance;
import com.leadingsoft.controller.parse.DownloadHtmlDocument;
import com.leadingsoft.ui.BrowserFrame;

/**
 * @ClassName UrlBrowserInstance
 * @Description Url浏览器实例<br>
 *              进行数据抓取<br>
 *              下一页是通过修改浏览器地址进行获取
 * @author gongym
 * @date 2018年6月7日 下午5:09:30
 */
public class UrlBrowserInstance implements BrowserInstance {
	private CefClient nowClient;
	private Boolean nowOsrEnabled;
	private Boolean nowTransparentPaintingEnabled;

	public UrlBrowserInstance(CefClient client, boolean osrEnabled, boolean transparentPaintingEnabled) {
		nowClient = client;
		nowOsrEnabled = osrEnabled;
		nowTransparentPaintingEnabled = transparentPaintingEnabled;
	}
	/**
	 * @Title createBrowserAndCrawler
	 * @Description 创建浏览器实例
	 * @param startIndex
	 * @param stopIndex
	 * @return void
	 */
	@Override
	public void createBrowserAndCrawler(Long startIndex, Long stopIndex) {
		// 给一个起始页创建浏览器对象
		CefBrowser browser = nowClient.createBrowser("http://www.baidu.com", nowOsrEnabled,
				nowTransparentPaintingEnabled);
		nowClient.addLoadHandler(new CefLoadHandlerAdapter() {
			@Override
			public void onLoadEnd(CefBrowser browser, CefFrame frame, int what) {
				String url = browser.getURL();
				if (CheckUrl.isListUrl(url)) {
					String index = StringUtils.remove(url, Constant.CTRIPHOTELLISTUTLTEMP);
					DownloadHtmlDocument htmlSource = new DownloadHtmlDocument(url);
					browser.getSource(htmlSource);
					Long nextIndex = Long.parseLong(index) + 1;
					if (nextIndex <= stopIndex) {
						StringBuilder nextUrl = new StringBuilder(Constant.CTRIPHOTELLISTUTLTEMP);
						nextUrl.append(nextIndex);
						TaskQuene.taskUrl.add(nextUrl.toString());
					} else {
						// 当前任务结束
						// 关闭浏览器
						browser.close();
					}
				} else {
					StringBuilder startUrl = new StringBuilder(Constant.CTRIPHOTELLISTUTLTEMP);
					startUrl.append(startIndex);
					TaskQuene.taskUrl.add(startUrl.toString());
				}
			}
		});
		final BrowserFrame frame = new BrowserFrame(nowClient, browser);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				frame.dispose();
				CefApp.getInstance().dispose();
			}
		});
		frame.setSize(1000, 500);
		frame.setVisible(true);
		// 开始抓取数据
		crawlerData(browser);
	}
	/**
	 * @Title crawlerData
	 * @Description 循环列表抓取数据
	 * @param startIndex
	 * @param stopIndex
	 * @return void
	 */
	private void crawlerData(CefBrowser browser) {
		while (true) {// 循环调用，如果队列中有任务就会加载页面
			String ctripHotelListUrl = TaskQuene.taskUrl.poll();
			if (null != ctripHotelListUrl) {
				// 加载页面
				browser.loadURL(ctripHotelListUrl);
			}
		}
	}
}
