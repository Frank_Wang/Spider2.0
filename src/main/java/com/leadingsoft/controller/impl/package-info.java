/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title package-info.java 
 * @Prject Spider2.0
 * @Package com.leadingsoft.controller.impl 
 * @Description 新版爬虫，使用Java调用浏览器引擎
 * @author gongym   
 * @date 2018年6月5日 下午6:45:09 
 * @version V1.0   
 */
/**
 * @ClassName package-info
 * @Description 解析页面文档的实现类集合
 * @author gongym
 * @date 2018年6月5日 下午6:45:09
 */
package com.leadingsoft.controller.impl;
